# ScorpionUI service: User behavior tracker

This service uses the [TA3/web-user-behavior](https://github.com/TA3/web-user-behaviour) library to track user behavior.
